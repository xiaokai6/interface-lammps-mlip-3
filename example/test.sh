#/bin/bash

# Preamble, common for all examples
LMP_EXE=../lmp_mpi 
TMP_DIR=./out
mkdir -p $TMP_DIR

# Body:
mpirun -n 3 $LMP_EXE -in in.my
cp log.lammps $TMP_DIR
cp dump.trj $TMP_DIR
rm -f log.lammps
rm -f dump.trj
